
# Team Reflection

I would suggest our whole team needs to have involvement on how we will win work.
It is helpful in development to spent part of time attempting to sell our activities to others.
Could be tender responses, presentation creation and review.

It would be nice to have a visible backlog of common (which are related to most projects) tasks/capabilities that we review once a week to discuss how the capabilities are evolving.

Our team doesnt seem to have review session and I think periodic reflection by the team would be a good thing.

I am seeing activities in other teams where people who are not doing the work are estimating it.
Where possible we should get people who are going to be doing the work to estimate it.

Like I am seeing in other teams that work gets broken down by people who are not doing the work.
Generally this can be an issue and for our team getting people who are doing the work to break it down would be a good thing.

I think we need to rotate work through more people rather than having 1 person do everything with regards to one project.
If we can to have more commonality overlap with our project we should be doing this to acheive standardization even if it slows the work down somewhat.

I think we need a bit more focus on what we think products will need
- styling is important and being given some attention now
- but why not also CI/CD
- a deployment architecture
- authentication
I think there needs to be continual improvement with these capabilities as well.

Another thing I have seen many projects do in Aurecon is to price up delivery assuming specific individual or teams do the work.
I think we should be pricing work based on a blended rate for the team doing the work such that anyone in a team can contribute to the work.
This will enable more standardization.

Do have a backlog board where work is deposited without being assigned to individuals
Let people pick up activites from the board rather than getting people to assign work to other people

Another thing I thing we should do more is break work down in a team and discuss with the team without assigning to individuals.
As people become free they can pick up tasks which are available to work on.
Discussing work as a team before doing it helps to keep people on the same page. 

Build software libraries by using them as they are being built.
It is very risky to build libraries without those libraries being used.
Library development requires extra collaboration and viewpoints to ensure the functionality is robust and applicable.

I think as a team we need more interaction with potential end users of a software system.
Without this contact developers are left guessing as to what is important about the software being produced.
We should bring external stakeholders into team meeting to present planned work.
This would be helpful to focus on what is important about solutions to be delivered.

People tend to assume that role X should do some activity.
We tend to compartmentalize people into boxes and then it is assumed that some activity requires a particular person. 
This creates issues were work is held up for a particular individual.

People who raise tickets should be responsible for the ownership of the ticket through to its conclusion...including closing them.
Tickets tend to get created which are not well detailed and then someone else has to decide if they are applicable and close tickets.

I'd like to try a bit more structure in the working week around the capabilities being developed.
So while there is focus on project work, there is also some focus on improvements in capabilities.

Our team needs to have a session on educating project managers about software development.
We tend to get PMs who dont necessarily have software development experience. 
They tend to get into trouble in the following ways
- they ask for estimates (they should only focus on priorities)
- they ask for estimates of the wrong person
- put dates against outcomes without considering all the subtasks

